package univ.rouen.model;

public class MonBean {
    private final String value;

    public MonBean() {
        this(null);
    }

    public MonBean(String value) {
        this.value = value;
    }

    public void foo() {
        System.out.println("Bean: foo is calling");
    }

    public void fooWithArg(String value) {
        System.out.println("Bean: fooWithArg is calling: " + value);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MonBean{");
        sb.append("value='").append(value).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
