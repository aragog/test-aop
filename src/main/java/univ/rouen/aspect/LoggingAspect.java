package univ.rouen.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import univ.rouen.model.MonBean;

@Aspect
public class LoggingAspect {
    @Before("dansLePaquetModel() && executionDeFoo()")
    private void logBefore() {
        System.out.println("logBefore() is running!");
    }

    @After("dansLePaquetModel() && executionDeFooWithArg() && args(value)")
    private void logAfterWithArg(String value) {
        System.out.println("logAfterWithArg(): value is '" + value + "'");
    }

    @After("dansLePaquetModel() && executionDeFoo() && target(monBean)")
    private void logAfterOnTarget(MonBean monBean) {
        System.out.println("logAfterOnTarget(): value is '" + monBean + "'");
    }

    @After("dansLePaquetModel()")
    private void logWithJoinPoint(JoinPoint joinPoint) {
        String method = joinPoint.getSignature().getName();
        int nbArgs = joinPoint.getArgs().length;
        Object target = joinPoint.getTarget();
        System.out.println(method + ": " + nbArgs + " on " + target);
    }

    @Pointcut("execution(void univ.rouen.model.MonBean.foo())")
    private void executionDeFoo() {}

    @Pointcut("execution(void univ.rouen.model.MonBean.fooWithArg(..))")
    private void executionDeFooWithArg() {}

    @Pointcut("within(univ.rouen.model.*)")
    private void dansLePaquetModel() {}
}
