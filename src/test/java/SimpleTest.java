import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import univ.rouen.model.MonBean;

@ContextConfiguration("/spring.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class SimpleTest {
    @Autowired
    private MonBean monBean;
    @Autowired
    private MonBean monBeanAvecProp;

    @Test
    public void test() {
        monBean.foo();
        monBean.fooWithArg("Mon argument");
        System.out.println();
        monBeanAvecProp.foo();
    }
}
