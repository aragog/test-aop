# Description #

Application prête à exécuter - M2 GIL (université de Rouen)

Permet de tester un projet configuré pour Spring AOP couplé aux annotations AspectJ comme vu en cours.

# Liens #

[Doc Spring](http://docs.spring.io/autorepo/docs/spring/4.1.0.RELEASE/spring-framework-reference/html/aop.html).

[Doc AspectJ](http://www.eclipse.org/aspectj/doc/released/progguide/index.html).